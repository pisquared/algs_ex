def qsort(ar):
    if len(ar) < 2:
        return ar
    pivot = ar[0]
    less = [i for i in ar[1:] if i <= pivot]
    greater = [i for i in ar[1:] if i > pivot]
    return qsort(less) + [pivot] + qsort(greater)

print qsort([7,5,6,1,2,4,3])
