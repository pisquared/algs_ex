def compress(s):
    compressed = []
    # Assign an initial integer code to each possible character. This is the initial codebook
    codebook = dict()
    for i in range(127):
        codebook[chr(i)] = i
    nextint = 128
    # Keep a buffer of the last remembered substring, which is initially empty
    buf = ""
    # Iterate through each character of the string to compress.
    for char in s:
        # Keep appending characters to the buffer until there is no codebook entry for the buffer.
        buf += char
        if buf not in codebook:
            # Add a new code for the buffer to the codebook.
            codebook[buf] = nextint
            nextint += 1
            # Append the integer code for the buffer without the last character to the compressed output. By definition, this code must be in the codebook.
            compressed.append(codebook[buf[:-1]])
            # clear the buffer
            buf = char
    compressed.append(codebook[buf])
    return compressed, codebook

def decompress(compressed, codebook):
    s = ""
    rev_codebook = dict()
    for k, v in codebook.iteritems():
        rev_codebook[v] = k
    for code in compressed:
        s += rev_codebook[code]
    return s


# TEST #
## This should run without errors
import traceback # note that this import is permitted
try:
    print compress("abcd")[0]
    print compress("01234")[0]
    print compress("aaaaaaaa")[0]
except:
    traceback.print_exc()
    print "Printing test failed"
print "Printing test OK"


test_strings = ["aaaaaa", "abcracdabra",
                "Independence day",
               "The quick brown fox jumped over the lazy dog",
               "b", "........", "-=-==-=-===-=-====",
               "\x00\x01\x02\x02\x02\x00",
                "A rose is a rose is a rose"]
try:
    for string in test_strings:
        if decompress(*compress(string))!=string:
            print "Failed decompress/compress  testing on %s" % string
            break
    print "All decompress/compress tests passed OK"
except:
    traceback.print_exc()
    print "Failed decompress/compress  testing on %s" % string


try:
    assert(len(compress("abcdef")[0])==6)
    assert(len(compress("aaaaaa")[0])==3)
    assert(len(compress("aaaaaaba")[0])==5)
    assert(len(compress("hlo helo hello heeeellllooo")[0])==18)
    assert(len(compress("::string:: ::strength:: ///")[0])==20)
    print "Length tests passed OK"
except:
    traceback.print_exc()
    print "Length tests failed"

# These tests may fail if you have used a different encoding
# Failure on this specific test does not necessarily imply your solution
# is incorrect
try:
    assert(compress("abcd")[0]==[97, 98, 99, 100])
    assert(compress("01234")[0]==[48, 49, 50, 51, 52])
    assert(compress("0123401")[0]==[48, 49, 50, 51, 52, 128])
    assert(compress("aaaaaaaa")[0]==[97, 128, 129, 128])
    assert(compress("hello hello hello")[0]==[104, 101, 108, 108, 111, 32, 128, 130, 132, 134, 131])
    print "All tests passed OK"
except:
    traceback.print_exc()
    print "Compression coding test failed"

################################################################
#                       TESTING
################################################################
# Verify that this cell runs correctly
# This will be used to test your code against a hidden test set
import traceback
try:
    with open("secret_test.txt") as f:
        with open("secret_test_output.txt", "w") as out_f:
            for line in f:
                test = line.strip()
                compressed, codebook = compress(test)
                out_f.write("%s\t%s\t%s\n" % (len(compressed), decompress(compressed, codebook), decompress(compressed,codebook)==test))
        print "Secret testing works OK"
except:
    traceback.print_exc()
    print "Secret testing failed"
