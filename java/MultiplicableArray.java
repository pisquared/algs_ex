/**
Имате масив с N числа и искате да върнете нов масив, в който на i-та позиция е записано произведението на всички числа от входния масив, с изключение на i-тото. Как бихте решили задачата, ако нямахте деление (примерно трябва да върнете числата по някакъв модул)?
*/
import java.util.Arrays;

public class MultiplicableArray {
  public static int[] productAll(int[] input) {
    if (input.length <= 1) {
      return input;
    }
    int[] fromLeft = new int[input.length];
    int[] fromRight = new int[input.length];
    int[] result = new int[input.length];
    fromLeft[0] = 1;
    fromRight[input.length - 1] = 1;
    for (int i = 1; i < input.length; i++ ) {
      fromLeft[i] = fromLeft[i - 1] * input[i - 1];
    }
    for (int i = input.length - 2; i >= 0; i--) {
      fromRight[i] = fromRight[i + 1] * input[i + 1];
    }
    for (int i = 0; i < input.length; i++ ){
      result[i] = fromRight[i] * fromLeft[i];
    }
    return result;
  }

  public static void main(String[] args) {
    int[] array = {1, 2};
              // { 24, 12, 8, 6}
    System.out.println(Arrays.toString(productAll(array)));
  }
}
