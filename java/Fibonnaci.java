public class Fibonnaci {
  public static int recursive(int n) {
    if (n == 0 || n == 1) {
      return n;
    }
    return recursive(n - 1) + recursive(n - 2);
  }

  public static int dynamic(int n) {
    int[] memoized = new int[n + 1];
    memoized[0] = 0;
    memoized[1] = 1;
    for (int i = 2; i <= n; i++) {
      memoized[i] = memoized[i-2] + memoized[i-1];
    }
    return memoized[n];
  }

  public static void main(String[] args) {
    int n = 17;
    System.out.println(recursive(n));
    System.out.println(dynamic(n));
  }
}
