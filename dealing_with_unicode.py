# -*- coding: utf-8 -*-

# TL;DR: If you use utf-8 encoding of Unicode code points,
# these are the rules you need:
# str - encode('utf-8') -> unicode
# unicode - decode('utf-8') -> str

# The first line (important bit coding: utf-8) declares that this file
# encoding is in utf-8
# By default python encodes everything in ASCII. So if we don't type this
# in line 1 or 2, python will complain reading the file itself.

# Let's say we have the letter A.
# We can represent it in these 4 variants:
# 'A'       -> String type, literal representation
# '\x41'    -> String type, \x escaped sequence
# u'A'      -> Unicode type, literal representation
# u'\u0041' -> Unicode type, \u escaped sequence

# Note: If we try to print a str() typed '\u0041' which is unicode escaped,
# we will just get the literal characters '\' 'u' '0' '0' '4' '1'
# See the difference here:
# >>> print [a for a in '\u0041']
# ['\\', 'u', '0', '0', '4', '1']
# >>> print [a for a in u'\u0041']
# [u'A']

# So now that we have covered simple things like A, let's go to emojis.
# Here are 4 different representations of the heart emoji - ❤
# BINARY: 11100010 10011101 10100100 (1110xxxxx 10xxxxxx 10xxxxxx)
# HEX   :     0xE2     0x9D     0xA4 (e29da4)
# UTF-16:   0x2764

# Here are the string type representations.
# Note how the string encoded in ascii by default is actually 3 characters long
literal_str_heart = '❤'
escaped_str_heart = '\xe2\x9d\xa4'

# And here are the Unicode representations.
literal_unicode_heart = u'❤'
escaped_unicode_heart = u'\u2764'

# STR - To make a unicode -> DECODE('utf-8')
# '❤' and '\xe2\x9d\xa4'
print literal_str_heart
print escaped_str_heart
assert len(literal_str_heart) == 3 and \
       len(escaped_str_heart) == 3
# unicode(literal_str_heart)        -> UnicodeDecodeError: 'ascii' codec can't decode byte 0xe2
assert unicode(literal_str_heart, 'utf-8') == escaped_unicode_heart and \
       unicode(escaped_str_heart, 'utf-8') == escaped_unicode_heart
# literal_str_heart.encode('utf-8') -> UnicodeDecodeError: 'ascii' codec can't decode byte 0xe2
assert literal_str_heart.decode('utf-8') == escaped_unicode_heart and \
       escaped_str_heart.decode('utf-8') == escaped_unicode_heart
assert str(literal_str_heart) == escaped_str_heart

# UNICODE - To make a string -> ENCODE('utf-8').
# u'❤' and u'\u2764'
print escaped_unicode_heart
print literal_unicode_heart
assert type(literal_unicode_heart) is unicode and \
       type(escaped_unicode_heart) is unicode
assert len(literal_unicode_heart) == 1 and \
       len(escaped_unicode_heart) == 1
assert unicode(literal_unicode_heart) == escaped_unicode_heart and \
       unicode(escaped_unicode_heart) == escaped_unicode_heart
# unicode(literal_unicode_heart, 'utf-8') -> TypeError: decoding Unicode is not supported
assert literal_unicode_heart.encode('utf-8') == escaped_str_heart and \
       escaped_unicode_heart.encode('utf-8') == escaped_str_heart
# literal_unicode_heart.decode('utf-8') -> UnicodeEncodeError: 'ascii' codec can't encode character u'\u2764'
