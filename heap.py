class Heap(object):
    def __init__(self):
        self._array = []

    def _get_parent_index(self, index):
        return (index - 1) / 2

    def _get_left_child_index(self, index):
        return (index * 2) + 1

    def _get_right_child_index(self, index):
        return (index * 2) + 2

    def _get_parent(self, index):
        return self._array[self._get_parent_index(index)]

    def _get_left_child(self, index):
        child_index = self._get_left_child_index(index)
        if child_index < len(self._array):
            return self._array[child_index]
        return None

    def _get_right_child(self, index):
        child_index = self._get_right_child_index(index)
        if child_index < len(self._array):
            return self._array[child_index]
        return None

    def _perculate_up(self, index=-1):
        if index == -1:
            index = len(self._array) - 1
        if index <= 0:
            return
        element = self._array[index]
        parent_index = self._get_parent_index(index)
        parent = self._array[parent_index]
        if parent > element:
            self._array[parent_index], self._array[index] = element, parent
            self._perculate_up(parent_index)

    def _get_min_child_index(self, index):
        left_child = self._get_left_child(index)
        if left_child:
            right_child = self._get_right_child(index)
            if right_child and right_child < left_child:
                return self._get_right_child_index(index)
            else:
                return self._get_left_child_index(index)

    def _perculate_down(self, index=0):
        if len(self._array) < 1:
            return  
        element = self._array[index]
        child_index = self._get_min_child_index(index)
        if child_index and child_index < len(self._array):
            child = self._array[child_index]
            parent_index = self._get_parent_index(index)
            if element > child:
                self._array[index], self._array[child_index] = child, element
                self._perculate_down(child_index)

    def add(self, element):
        self._array.append(element)
        self._perculate_up()

    def remove_min(self):
        element = self._array[0]
        self._array[0], self._array[-1] = self._array[-1], self._array[0]
        if len(self._array) > 0:
            self._array = self._array[:-1]
        index = len(self._array) - 1
        self._perculate_down()
        return element

    def peek(self):
        return self._array[0]

h = Heap()

h.add(4)
h.add(6)
h.add(7)
h.add(3)
h.add(8)
h.add(2)
h.add(5)
h.add(1)

assert h.remove_min() == 1
assert h.remove_min() == 2
assert h.remove_min() == 3
assert h.remove_min() == 4
assert h.remove_min() == 5
assert h.remove_min() == 6
assert h.remove_min() == 7
assert h.remove_min() == 8
