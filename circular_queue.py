class CQueue(object):
    """
    Circular Queue with specified capacity.
    If full, overwrite.
    If empty, return None.
    """
    def __init__(self, capacity):
        self.capacity = capacity
        self._array = [0] * capacity
        self._start = 0
        self._end = 0
        self._size = 0

    def add(self, element):
        if self.is_full():
            raise Exception('Queue full')
        self._array[self._end] = element
        self._end = (self._end + 1) % self.capacity
        self._size += 1

    def remove(self):
        if self.is_empty():
            return None
        element = self._array[self._start]
        self._start = (self._start + 1) % self.capacity
        self._size -= 1
        return element

    def is_empty(self):
        return self._start == self._end and self._size == 0

    def is_full(self):
        return self._size == self.capacity

    def length(self):
        return self._size

cq = CQueue(3)
assert cq.is_empty() is True
assert cq.is_full() is False
assert cq.length() == 0

cq.add('a')
# ['a', 0, 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 1

cq.add('b')
# ['a', 'b', 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 2

assert cq.remove() == 'a'
# [0, 'b', 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 1

cq.add('c')
# [0, 'b', 'c']
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 2

cq.add('d')
# ['d', 'b', 'c']
assert cq.is_empty() is False
assert cq.is_full() is True
assert cq.length() == 3

try:
    cq.add('e')
    assert False
except Exception as e:
    assert e.message == 'Queue full'

assert cq.remove() == 'b'
# ['d', 0, 'c']
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 2

assert cq.remove() == 'c'
# ['d', 0, 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 1

cq.add('e')
# ['d', 'e', 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 2

assert cq.remove() == 'd'
# [0, 'e', 0]
assert cq.is_empty() is False
assert cq.is_full() is False
assert cq.length() == 1

assert cq.remove() == 'e'
# [0, 0, 0]
assert cq.is_empty() is True
assert cq.is_full() is False
assert cq.length() == 0

assert cq.remove() is None
