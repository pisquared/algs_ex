# coding: utf-8
# http://www.informatika.bg/interviews#TextAndStrings
# Имате (потенциално много дълъг) текст и три стринга S1, S2, и S3.
# Намерете най-късия откъс от текста, съдържащ и трите стринга. Редът на стринговете няма значение.

txt = """lorem ipsum dolor sit amet sit amet dolor ipsum ipsum dolor"""

s1 = "ipsum"
s2 = "dolor"
s3 = "sit"

left = 0
right = 0

is_beginning_of = []
is_end_of = []

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1:
            return
        yield start
        start += len(sub)

for s in [s1, s2, s2]:
    all_indexes_b = [a for a in find_all(txt, s)]
    isb = [False if x not in all_indexes_b else True for x in range(len(txt))]
    is_beginning_of.append(isb)

    all_indexes_e = [a + len(s) - 1 for a in all_indexes_b]
    ise = [False if x not in all_indexes_e else True for x in range(len(txt))]
    is_end_of.append(ise)

ans = len(txt) + 1
cnt = [0,0,0]
while right<len(txt):
    print "r: %s" % right
    for i in range(3):
        if is_end_of[i][right]:
            cnt[i] += 1
    while left <= right and cnt[0] > 0 and cnt[1] > 0 and cnt[2] > 0:
        ans = min(ans, right - left + 1)
        for j in range(3):
            if is_beginning_of[j][left]:
                cnt[j] -= 1
        left += 1
        print " l: %s" % left
    right += 1
    print txt[left:right]

print ans
