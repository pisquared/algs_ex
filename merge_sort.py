def merge_sort(a, depth=0):
    print ' ' * depth + "sorting %s " % a
    if len(a) < 2:
        print ' ' * depth + 'rv %s' % a
        return a
    elif len(a) == 2:
        if a[0] > a[1]:
            a[1], a[0] = a[0], a[1]
        print ' ' * depth + 'rv %s' % a
        return a

    middle = len(a) / 2
    left = merge_sort(a[:middle], depth=depth + 1)
    right = merge_sort(a[middle:], depth=depth + 1)

    rv = [0] * (len(left) + len(right))
    li, ri = 0, 0
    for i in range(len(left) + len(right)):
        if len(left) > li and len(right) > ri:
            if left[li] < right[ri]:
                rv[i] = left[li]
                li += 1
            else:
                rv[i] = right[ri]
                ri += 1
        elif len(left) <= li:
            rv[i:] = right[ri:]
            break
        else:
            rv[i:] = left[li:]
            break
    print ' ' * depth + 'rv %s' % rv
    return rv

print merge_sort([1,4,8,6,3,2,5])
