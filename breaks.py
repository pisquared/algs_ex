# A "journey" is a sequence of flights (at least one). Example:
# SOF-BRU-LON-NYC
# A journey can be broken into one or more "tickets", such that
# a ticket can contain one or more sequential flights. For example
# the above journey can be broken into tickets in the following ways:
#
# breakup 1:
#     ticket 1 - SOF-BRU-LON-NYC
# breakup 2:ш
#     ticket 1 - SOF-BRU-LON
#     ticket 2 - LON-NYC
# breakup 3:
#     ticket 1 - SOF-BRU
#     ticket 2 - BRU-LON-NYC
# breakup 4:
#     ticket 1 - SOF-BRU
#     ticket 2 - BRU-LON
#     ticket 3 - LON-NYC
#
# Provide a code snippet(in a language by your choice) that given a number of flights N generates
# all possible breakups of a journey with N flights into tickets.
# A breakup consists of the indices of the flights that comprise the ticket.
# For the above example:
# N=3
# ---- 00
# 0 1 2
# ---- 01
# 0 1
# 2
# ---- 10
# 0
# 1 2
# ---- 11
# 0
# 1
# 2

# N=4
# -----    000 0
# 0 1 2 3
# ----     001 1
# 0 1 2
# 3
# ----     010 2
# 0 1
# 2 3
# ------   011 3
# 0 1
# 2
# 3
# -----    100 4
# 0
# 1 2 3
# -----    101 5
# 0
# 1 2
# 3
# ------   110 6
# 0
# 1
# 2 3
# ------   111 7
# 0
# 1
# 2
# 3

def breakup(n):
    # Count the breaks as binary 0/1 - should break, or should not break (see examples above)
    # break points are n - 1, e.g.: 0 [br_1] 1 [br_2] 2 [br_3] 3
    # the max binary number with n digits is (2^(n-1))-1 but range already removes last element
    max_bin, bin_fmt = 2**(n - 1), '0{}b'.format(n - 1)
    for bi in range(max_bin):
        b = format(bi, bin_fmt)
        print("\n--- Breakup {}".format(bi))
        for i in range(n):
            print(i, "", end="")
            if i < n-1 and b[i] == '1':
                print()  # break is needed, new line

if __name__ == "__main__":
    breakup(4)
